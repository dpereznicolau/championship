self.addEventListener('install', function(e) {
    e.waitUntil(
      caches.open('championship').then(function(cache) {
        return cache.addAll([
          '/torneiglol/static/offline.html'
        ]);
      })
    );
   });
   
   self.addEventListener('fetch', function(e) {
     console.log(e.request.url);
     e.respondWith(
        fetch(e.request).catch(function(){
            return caches.match('offline.html')
        })
     );
   });
