$(function () {
    'use strict';

    $(document).on('click', '.matchResultBtn', function(){
        var form = $(this).next();
        form.toggle();
    });

    $('.groupTable').DataTable({
        "searching": false,
        "paging": false,
        "info": false,
        "columnDefs": [
            { className: 'text-left', targets: [0] },
            { className: 'text-center', targets: [1, 2, 3, 4] },
            { visible : false, targets: [5] }
        ],
        "order": [[4, "desc"], [5, "desc"]]
    });

    $('.dbTable').DataTable();

    $('.roundTable').DataTable({
        "searching": false,
        "paging": false,
        "info": false
    });

    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
                 .register('/torneiglol/sw.js')
                 .then(function() { console.log('Service Worker Registered'); });
    }
    
    let deferredPrompt;
    const addBtn = document.querySelector('.add-button');
    addBtn.style.display = 'none';
    
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later.
      deferredPrompt = e;
      // Update UI to notify the user they can add to home screen
      addBtn.style.display = 'inline-block';
    
      addBtn.addEventListener('click', (e) => {
        // hide our user interface that shows our A2HS button
        addBtn.style.display = 'none';
        // Show the prompt
        deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
              console.log('User accepted the Championship prompt');
            } else {
              console.log('User dismissed the Championship prompt');
            }
            deferredPrompt = null;
          });
      });
    });
});