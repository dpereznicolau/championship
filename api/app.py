import cx_Oracle
from flask import Flask, render_template, request, g, redirect, url_for, session


app = Flask(__name__)
app.config.from_object('api.default_settings')
try:
    app.config.from_envvar('CHAMPIONSHIP_SETTINGS')
except RuntimeError as e:
    app.logger.warn('Could not set settings from environment: %s', e)


def get_db():
    if 'db' not in g:
        g.db = cx_Oracle.SessionPool(
            app.config['ORA_USER'],
            app.config['ORA_PASSWORD'],
            app.config['ORA_DSN'],
            events=True,
            threaded=True
        )
    return g.db.acquire()


@app.route('/')
def index():
    with get_db() as db:
        cursor = db.cursor()

        groups = {}
        for team in cursor.execute("""select m.id_group, t.id, t.name,
                                        m.matches_played, m.matches_won, m.matches_lost, m.total_points, m.tie_breaker
                                        from groups_teams m, teams t
                                        where m.id_team = t.id"""):
            groupId = team[0]
            if not groupId in groups:
                groups[groupId] = {
                    'teams': [],
                    'rounds': {},
                }

            groups[groupId]['teams'].append({
                'teamName': team[2],
                'played': team[3],
                'won': team[4],
                'lost': team[5],
                'points': team[6],
                'tie_breaker' : team[7]
            })

        for match in cursor.execute("""select m.id_group, m.round, m.id_team1, t1.name, m.id_team2, t2.name, m.result
                                    from matches m, teams t1, teams t2
                                    where m.id_team1 = t1.id
                                    and m.id_team2 = t2.id
                                    order by m.id_group, m.round"""):
            groupId = match[0]

            if match[6] == '1':
                result = match[3] + ' WON'
            elif match[6] == '2':
                result = match[5] + ' WON'
            else:
                result = 'Pending'

            round = match[1]
            if not round in groups[groupId]['rounds']:
                groups[groupId]['rounds'][round] = {
                    'matches': []
                }

            groups[groupId]['rounds'][round]['matches'].append({
                'id_team1': match[2],
                'name_team1': match[3],
                'id_team2': match[4],
                'name_team2': match[5],
                'result': result
            })

        brackets = {
            0 : 'Final',
            1 : 'Semifinal',
            2 : 'Quarts',
            3 : 'Vuitens'
        }

        finals = {}
        cursor.execute("Select max(id) from finalmatches")
        temp = cursor.fetchone()[0]
        idFinal = int(temp) if temp else None

        def generateBrackets(depth, id) :
            cursor.execute("""Select f.id, f.idparent1, f.idparent2,
                                nvl(t1.name,'Undefined'), nvl(t2.name,'Undefined'), f.result
                                From finalmatches f
                                left join teams t1
                                on f.id_team1 = t1.id
                                left join teams t2
                                on f.id_team2 = t2.id
                                Where f.id = :id""", [id])
                
            row = cursor.fetchone()
            if not row :
                return None
                
            return {
                'name' : brackets[depth],
                'team1' : row[3],
                'team2' : row[4],
                'children' : list(filter(None, [generateBrackets(depth+1, row[1]), generateBrackets(depth+1, row[2])]))
            }

        finals = generateBrackets(0, idFinal)

    return render_template('index.html', groups=groups, finals=finals)

@app.route('/login', methods=['GET', 'POST'])
def login():
    response = True
    if request.method == 'POST':
        userId = request.form['userId']
        password = request.form['password']

        with get_db() as db:
            cursor = db.cursor()

            cursor.execute("Select count(*) from users where userId = :userId and password = :password", [userId, password])
            resp = int(cursor.fetchone()[0])

            if resp > 0 :
                session['userId'] = userId
                return redirect(url_for('admin'))
            else :
                response = False

    return render_template('login.html', response=response)

@app.route('/logout')
def logout():
    session.pop('userId', None)
    return redirect(url_for('login'))

@app.route('/admin')
def admin():
    if 'userId' not in session:
        return redirect(url_for('login'))

    with get_db() as db:
        cursor = db.cursor()

        groups = []

        for row in cursor.execute("select id, name from groups order by id"):
            groups.append({
                'id': row[0],
                'name': row[1]
            })

        teams = []
        for row in cursor.execute("select id, name from teams"):
            teams.append({
                'id': row[0],
                'name': row[1]
            })

        groups_teams = []
        for row in cursor.execute("""select g.id, g.name, t.id, t.name,
                                    gt.matches_played, gt.matches_won, gt.matches_lost, gt.total_points, gt.tie_breaker
                                    from groups_teams gt, groups g, teams t
                                    where gt.id_group = g.id
                                    and gt.id_team = t.id
                                    order by g.id"""):
            groups_teams.append({
                'id_group': row[0],
                'name_group': row[1],
                'id_team': row[2],
                'name_team': row[3],
                'matches_played': row[4],
                'matches_won': row[5],
                'matches_lost': row[6],
                'total_points': row[7],
                'tie_breaker' : row[8]
            })

        matches = []
        for row in cursor.execute("""select m.id_match, m.id_group, g.name, m.round,
                                    m.id_team1, t1.name, m.id_team2, t2.name, m.result, m.tie_breaker_t1, m.tie_breaker_t2
                                    from matches m, groups g, teams t1, teams t2
                                    where m.id_group = g.id
                                    and m.id_team1 = t1.id
                                    and m.id_team2 = t2.id
                                    order by m.id_group, m.round, m.id_match"""):
            matches.append({
                'id_match': row[0],
                'id_group': row[1],
                'name_group': row[2],
                'round': row[3],
                'id_team1': row[4],
                'name_team1': row[5],
                'id_team2': row[6],
                'name_team2': row[7],
                'result': row[8],
                'team1_tb' : row[9],
                'team2_tb' : row[10]
            })

        finals = []
        for row in cursor.execute("""select f.id, f.idparent1, f.idparent2, f.id_team1, t1.name, f.id_team2, t2.name, f.result
                                    from finalmatches f
                                    left join teams t1 on f.id_team1 = t1.id
                                    left join teams t2 on f.id_team2 = t2.id"""):
            finals.append({
                'id_final' : row[0],
                'id_parent1' : row[1],
                'id_parent2' : row[2],
                'id_team1' : row[3],
                'name_team1' : row[4],
                'id_team2' : row[5],
                'name_team2' : row[6],
                'result' : row[7]
            })

    return render_template('admin.html', groups=groups, teams=teams, groups_teams=groups_teams, matches=matches, finals=finals)


@app.route('/insertGroup', methods=['POST'])
def insertGroup():
    with get_db() as db:
        cursor = db.cursor()

        cursor.execute('Select max(id)+1 from groups')
        groupId = int(cursor.fetchone()[0])
        groupName = request.form['groupName']

        cursor.execute("INSERT INTO groups (id, name) VALUES (:groupId, :groupName)", [
                       groupId, groupName])

        db.commit()
    return redirect(url_for('admin'))


@app.route('/deleteGroup', methods=['POST'])
def deleteGroup():
    groupId = int(request.form['groupId'])
    with get_db() as db:
        cursor = db.cursor()
        cursor.execute("DELETE FROM groups WHERE id = :groupId", [groupId])
        db.commit()
    return redirect(url_for('admin'))


@app.route('/insertTeam', methods=['POST'])
def insertTeam():
    with get_db() as db:
        cursor = db.cursor()

        cursor.execute('Select max(id)+1 from teams')
        teamId = int(cursor.fetchone()[0])
        teamName = request.form['teamName']

        cursor.execute("INSERT INTO teams (id, name) VALUES (:teamId, :teamName)", [
                       teamId, teamName])
        db.commit()
    return redirect(url_for('admin'))


@app.route('/deleteTeam', methods=['POST'])
def deleteTeam():
    teamId = int(request.form['teamId'])
    with get_db() as db:
        cursor = db.cursor()
        cursor.execute("DELETE FROM teams WHERE id = :teamId", [teamId])
        db.commit()
    return redirect(url_for('admin'))


@app.route('/insertGroupTeam', methods=['POST'])
def insertGroupTeam():
    groupId = int(request.form['groupId'])
    teamId = int(request.form['teamId'])
    with get_db() as db:
        cursor = db.cursor()
        cursor.execute("""INSERT INTO groups_teams (id_group, id_team, matches_played, matches_won, matches_lost, total_points)
                        VALUES (:groupId, :teamId, 0, 0, 0, 0)""", [groupId, teamId])
        db.commit()
    return redirect(url_for('admin'))


@app.route('/deleteGroupTeam', methods=['POST'])
def deleteGroupTeam():
    teamId = int(request.form['teamId'])
    groupId = int(request.form['groupId'])
    with get_db() as db:
        cursor = db.cursor()
        cursor.execute("DELETE FROM groups_teams WHERE id_group = :groupId and id_team = :teamId", [
                       groupId, teamId])
        db.commit()
    return redirect(url_for('admin'))


@app.route('/generateRounds', methods=['POST'])
def generateRounds():
    groupId = int(request.form['groupId'])
    with get_db() as db:
        cursor = db.cursor()

        cursor.execute(
            "DELETE FROM matches WHERE id_group = :groupId", [groupId])

        teams = []
        index_teams = 0
        for team in cursor.execute("select id_team from groups_teams where id_group = :groupId", [groupId]):
            teams.append(team[0])

        auxT = len(teams)
        impar = True if auxT % 2 != 0 else False

        if impar:
            auxT += 1

        totalP = int(auxT/2)  # total de partidos de una jornada
        jornada = []
        indiceInverso = auxT-2

        for i in range(1, auxT):
            for indiceP in range(0, totalP):
                if index_teams > auxT-2:
                    index_teams = 0

                if indiceInverso < 0:
                    indiceInverso = auxT-2

                if indiceP == 0:  # seria el partido inicial de cada fecha
                    if impar:
                        partido = [teams[index_teams], -1]  # descanso
                    else:
                        if (i+1) % 2 == 0:
                            partido = [teams[index_teams], teams[auxT-1]]
                        else:
                            partido = [teams[auxT-1], teams[index_teams]]
                else:
                    partido = [teams[index_teams], teams[indiceInverso]]
                    indiceInverso -= 1
                index_teams += 1

                cursor.execute('Select nvl(max(id_match)+1,0) from matches')
                matchId = int(cursor.fetchone()[0])

                cursor.execute("""INSERT INTO matches (id_match, id_group, round, id_team1, id_team2, result)
                        VALUES (:matchId, :groupId, :r, :team1Id, :team2Id, null)""", [matchId, groupId, i, partido[0], partido[1]])

        db.commit()

    return redirect(url_for('admin'))


@app.route('/setMatchResult', methods=['POST'])
def setMatchResult():
    matchId = int(request.form['matchId'])
    team1Id = int(request.form['team1Id'])
    team2Id = int(request.form['team2Id'])
    matchWinner = int(request.form['matchWinner'])

    if matchWinner == team1Id :
        result = '1'
    elif matchWinner == team2Id :
        result = '2'
    else :
        result = None

    with get_db() as db:
        cursor = db.cursor()

        cursor.execute(
            "Select id_group, result, nvl(tie_breaker_t1, 0), nvl(tie_breaker_t2, 0) from matches where id_match = :matchId", [matchId])
        row = cursor.fetchone()
        groupId = int(row[0])
        db_result = row[1]
        db_team1Tb = float(row[2])
        db_team2Tb = float(row[3])

        if request.form['team1Tb'] :
            team1Tb = float(request.form['team1Tb'])
        else :
            team1Tb = db_team1Tb
        
        if request.form['team2Tb'] :
            team2Tb = float(request.form['team2Tb'])
        else :
            team2Tb = db_team2Tb

        cursor.execute(
                """UPDATE matches
                    set result = :result,
                    tie_breaker_t1 = :tieT1,
                    tie_breaker_t2 = :tieT2
                    where id_match = :matchId""", [result, team1Tb, team2Tb, matchId])

        if db_result is None:
            # match sense resultat
            if result == '1' :
                cursor.execute("""UPDATE groups_teams
                                set matches_played = matches_played + 1,
                                matches_won = matches_won + 1,
                                total_points = total_points + 3,
                                tie_breaker = nvl(tie_breaker, 0) + :tieT1
                                where id_group = :groupId
                                and id_team = :team1Id""", [team1Tb, groupId, team1Id])
                cursor.execute("""UPDATE groups_teams
                                set matches_played = matches_played + 1,
                                matches_lost = matches_lost + 1,
                                tie_breaker = nvl(tie_breaker, 0) + :tieT2
                                where id_group = :groupId
                                and id_team = :team2Id""", [team2Tb, groupId, team2Id])
            elif result == '2' :
                cursor.execute("""UPDATE groups_teams
                                set matches_played = matches_played + 1,
                                matches_won = matches_won + 1,
                                total_points = total_points + 3,
                                tie_breaker = nvl(tie_breaker, 0) + :tieT2
                                where id_group = :groupId
                                and id_team = :team2Id""", [team2Tb, groupId, team2Id])
                cursor.execute("""UPDATE groups_teams
                                set matches_played = matches_played + 1,
                                matches_lost = matches_lost + 1,
                                tie_breaker = nvl(tie_breaker, 0) + :tieT1
                                where id_group = :groupId
                                and id_team = :team1Id""", [team1Tb, groupId, team1Id])
        elif db_result == result:
            # db_result no es null i es igual a result (act tie-breaker)
            cursor.execute("""UPDATE groups_teams
                                set tie_breaker = nvl(tie_breaker, 0) - :dbTieT2 + :tieT2
                                where id_group = :groupId
                                and id_team = :team2Id""", [db_team2Tb, team2Tb, groupId, team2Id])

            cursor.execute("""UPDATE groups_teams
                            set tie_breaker = nvl(tie_breaker, 0) - :dbTieT1 + :tieT1
                            where id_group = :groupId
                            and id_team = :team1Id""", [db_team1Tb, team1Tb, groupId, team1Id])

        else:
            # db_result no es null i es diferent a result (Actualitzar resultats)
            if result == '1' :
                cursor.execute("""UPDATE groups_teams
                            set matches_won = matches_won + 1,
                            matches_lost = matches_lost - 1,
                            total_points = total_points + 3,
                            tie_breaker = nvl(tie_breaker, 0) - :dbTieT1 + :tieT1
                            where id_group = :groupId
                            and id_team = :team1Id""", [db_team1Tb, team1Tb, groupId, team1Id])
                cursor.execute("""UPDATE groups_teams
                            set matches_won = matches_won - 1,
                            matches_lost = matches_lost + 1,
                            total_points = total_points - 3,
                            tie_breaker = nvl(tie_breaker, 0) - :dbTieT2 + :tieT2
                            where id_group = :groupId
                            and id_team = :team2Id""", [db_team2Tb, team2Tb, groupId, team2Id])
            elif result == '2' :
                cursor.execute("""UPDATE groups_teams
                            set matches_won = matches_won - 1,
                            matches_lost = matches_lost + 1,
                            total_points = total_points - 3,
                            tie_breaker = nvl(tie_breaker, 0) - :dbTieT1 + :tieT1
                            where id_group = :groupId
                            and id_team = :team1Id""", [db_team1Tb, team1Tb, groupId, team1Id])
                cursor.execute("""UPDATE groups_teams
                            set matches_won = matches_won + 1,
                            matches_lost = matches_lost - 1,
                            total_points = total_points + 3,
                            tie_breaker = nvl(tie_breaker, 0) - :dbTieT2 + :tieT2
                            where id_group = :groupId
                            and id_team = :team2Id""", [db_team2Tb, team2Tb,groupId, team2Id])
    
        db.commit()
    return redirect(url_for('admin'))


@app.route('/generateFinals', methods=['POST'])
def generateFinals():
    maxBracket = int(request.form['maxBracket'])
    if maxBracket == 0 :
        #Final
        nTeams = 2
    elif maxBracket == 1 :
        #Semifinal
        nTeams = 4
    elif maxBracket == 2 :
        #Quarter
        nTeams = 8
    else :
        #Eigth
        nTeams = 16
        maxBracket = 3

    with get_db() as db:
        cursor = db.cursor()
        #delete table
        cursor.execute("Delete finalmatches")

        cursor.execute("Select count(distinct(id_group)) from groups_teams")
        nGroups = int(cursor.fetchone()[0])
        
        nTeamsSel = nTeams // nGroups
        teams = []
        for i in range(1, nTeamsSel + 1) :
            cursor.execute("""Select id_team
                                        From (
                                            Select id_group, id_team, rank() over (partition by id_group order by total_points desc, tie_breaker desc, id_team) r
                                            From groups_teams) rnk
                                        Where r = :pos
                                        Order by id_group {}""".format('asc' if i % 2 == 0 else 'desc'), [i])
            teams.append([r[0] for r in cursor.fetchall()])

        idMatch = 0
        matchIDs = []
        while len(teams) > 0 :
            aTeams = teams.pop()
            if len(teams) > 0 :
                bTeams = teams.pop()
            else :
                bTeams = aTeams[len(aTeams)//2:]
                aTeams = aTeams[:len(aTeams)//2]
            
            matches = list(zip(aTeams, bTeams))

            for match in matches :
                cursor.execute("""Insert into finalmatches
                                    (id, id_team1, id_team2)
                                    Values (:idMatch, :idTeam1, :idTeam2)""", [idMatch, match[0], match[1]])
                matchIDs.append(idMatch)
                idMatch = idMatch + 1
        
        while len(matchIDs) > 1 :
            matchIDsAux = []
            while len(matchIDs) > 1 :
                parents = [matchIDs.pop(), matchIDs.pop()]
                cursor.execute("""Insert into finalmatches
                                    (id, idparent1, idparent2)
                                    Values (:idMatch, :idParent1, :idParent2)""", [idMatch, parents[0], parents[1]])
                matchIDsAux.append(idMatch)
                idMatch = idMatch + 1
            matchIDs = matchIDsAux
        
        db.commit()
    return redirect(url_for('admin'))

@app.route('/setFinalResult', methods=['POST'])
def setFinalResult():
    finalId = int(request.form['finalId'])
    team1Id = int(request.form['team1Id'])
    team2Id = int(request.form['team2Id'])
    finalWinner = int(request.form['finalWinner'])

    if finalWinner == team1Id :
        result = '1'
    elif finalWinner == team2Id :
        result = '2'
    else :
        result = None

    with get_db() as db:
        cursor = db.cursor()

        cursor.execute("""UPDATE finalmatches
                        set result = :result
                        where id = :finalId""", [result, finalId])

        cursor.execute("""select id, '1'
                            from finalmatches
                            where idparent1 = :finalId
                            union
                            select id, '2'
                            from finalmatches
                            where idparent2 = :finalId""", [finalId])
        row = cursor.fetchone()
        nextId = int(row[0])
        winner = row[1]

        if winner == '1' :
            cursor.execute("""UPDATE finalmatches
                        set id_team1 = :finalWinner
                        where id = :nextId""", [finalWinner, nextId])
        elif winner == '2' :
            cursor.execute("""UPDATE finalmatches
                        set id_team2 = :finalWinner
                        where id = :nextId""", [finalWinner, nextId])

        db.commit()
    return redirect(url_for('admin'))
    
@app.route('/manifest.webmanifest')
def pwa():
    return render_template('manifest.webmanifest')

@app.route('/sw.js')
def sw():
    return app.send_static_file('js/sw.js')